import React, { useState, useEffect } from 'react';

function LocationForm () {
  // Create the handleNameChange method to take what the user inputs
  // into the form and store it in the state's "name" variable.
  const [name, setName] = useState('')
  const handleNameChange = (event) => {
    // console.log("event", event)
    const value = event.target.value;
    setName(value);
  }

  const [roomCount, setRoomCount] = useState("")
  const handleRoomCountChange = (event) => {
    const value = event.target.value
    setRoomCount(value);
  }

  const [city, setCity] = useState('')
  const handleCityChange = event => {
    const value = event.target.value
    setCity(value);
  }

  const [state, setState] = useState("")
  const handleStateChange = event => {
    const value = event.target.value
    setState(value);
  }

  // console.log("states", states)
  // console.log("setStates", setStates)
  const [states, setStates] = useState([])

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
    data.name = name;
    data.city = city;
    data.state = state;
    data.room_count = roomCount;
    // console.log("data", data)

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json()
      // console.log("newLocation", newLocation)
      setName('');
      setRoomCount('');
      setCity('');
      setState('');
    }
  }




  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      // setStates is the functino that updates "states"
      // pass in what we want to update "states" wih inside "setStates()"
      setStates(data.states)

      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        const option = document.createElement('option');
        option.value = state.abbreviation;
        option.innerHTML = state.name;
        selectTag.appendChild(option);
      }
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form id="create-location-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input
                  placeholder="Name"
                  required type="text"
                  value={name}
                  name="name"
                  id="name"
                  className="form-control"
                  onChange={handleNameChange}

                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="Room count"
                  required type="number"
                  value={roomCount}
                  name="room_count"
                  id="room_count"
                  className="form-control"
                  onChange={handleRoomCountChange}
                />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  placeholder="City"
                  required type="text"
                  value={city}
                  name="city"
                  id="city"
                  className="form-control"
                  onChange={handleCityChange}
                />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select required value={state} name="state" id="state" className="form-select" onChange={handleStateChange}>
                  <option value="" >Choose a state</option>
                    {states.map(state => {
                      return (
                        <option key={state.abbreviation} value={state.abbreviation}>
                          {state.name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default LocationForm;
