import React, { useState, useEffect } from 'react';

function ConferenceForm() {

    const [name, setName] = useState("")
    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const [starts, setStarts] = useState('')
    const handleStartsChange = event => {
        const value = event.target.value
        setStarts(value)
    }

    const [ends, setEnds] = useState('')
    const handleEndsChange = event => {
        const value = event.target.value
        setEnds(value)
    }

    const [description, setDescription] = useState('')
    const handleDescriptionChange = event => {
        const value = event.target.value
        setDescription(value)
    }

    const [maxPresentations, setmaxPresentations] = useState('')
    const handleMaxPresentationsChange = event => {
        const value = event.target.value
        setmaxPresentations(value)
    }

    const [maxAttendees, setmaxAttendees] = useState('')
    const handleMaxAttendeesChange = event => {
        const value = event.target.value
        setmaxAttendees(value)
    }
    const [location, setLocation] = useState("")
    const handleLocationChange = event => {
        const value = event.target.value
        setLocation(value)
    }

    // const [location, setLocation] = useState([])
    const handleFormSubmit = async event => {
        event.preventDefault();
        const data = {}

        data.name = name;
        data.starts =  starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log("data", data)

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig)

        if (response.ok) {
            const newConference = await response.json()
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setmaxPresentations('');
            setmaxAttendees('');
            setLocation('');
        }
    }


    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/"

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            // console.log("data", data)

            const selectTag = document.getElementById('location')

            for (let location of data.locations) {
                const option = document.createElement('option')
                option.value = location["id"]
                option.innerHTML = location["name"]
                selectTag.appendChild(option)
            }
        }
    }
    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form" onSubmit={handleFormSubmit}>
              <div className="form-floating mb-3">
                <input
                    placeholder="Name"
                    required type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    onChange={handleNameChange}
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    placeholder="Starts"
                    required type="date"
                    name="starts"
                    id="starts"
                    className="form-control"
                    onChange={handleStartsChange}
                />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    placeholder="Ends"
                    required type="date"
                    name="ends"
                    id="ends"
                    className="form-control"
                    onChange={handleEndsChange}
                />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="room_count">Description</label>
                <textarea
                    name="description"
                    id="description"
                    rows="3"
                    className="form-control"
                    onChange={handleDescriptionChange}
                ></textarea>
              </div>
              <div className="form-floating mb-3">
                <input
                    placeholder="Maximum presentations"
                    required type="number"
                    name="max_presentations"
                    id="max_presentations"
                    className="form-control"
                    onChange={handleMaxPresentationsChange}
                />
                <label htmlFor="room_count">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    placeholder="Maximum attendees"
                    required type="number"
                    name="max_attendees"
                    id="max_attendees"
                    className="form-control"
                    onChange={handleMaxAttendeesChange}
                />
                <label htmlFor="room_count">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select
                    required name="location"
                    id="location"
                    className="form-select"
                    onChange={handleLocationChange}>
                  <option value="">Choose a location</option>
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm;
